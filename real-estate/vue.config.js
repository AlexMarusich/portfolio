module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "@/assets/scss/style.scss";'
      }
    }
  },

  publicPath: process.env.APP_BASE_PATH
}