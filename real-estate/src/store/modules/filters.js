export default {
  state: {
    filters: {},
  },

  mutations :{
    ASSIGN_FILTERS_FROM_QUERY (state, array) {
      Object.assign(state.filters, array)
    }
  },

  actions :{
    fillRequestsFromQuery ({commit}, array) {
      commit('ASSIGN_FILTERS_FROM_QUERY', array)
    },

    addEventFilter ({state, dispatch}, array) {
      array.forEach(item => {
        let key = (Object.keys(item))
        let value = (Object.values(item))
        state.filters[key] = value.join('')
      })
      dispatch('renderEstatesList', state.filters)
    },

    clearFilter ({state, dispatch}, string) {
      if (string === 'location') {
        delete state.filters[string]
        dispatch('renderEstatesList', state.filters)
      }
      if (string === 'area') {
        delete state.filters.area_gte
        delete state.filters.area_lte
        dispatch('renderEstatesList', state.filters)
      }
      if (string === 'price') {
        delete state.filters.price_gte
        delete state.filters.price_lte
        dispatch('renderEstatesList', state.filters)
      } else {
        state.filters = {}
        dispatch('renderEstatesList', state.filters)
      }
    }
  }
}