export default {
  state:{
    price:[]
  },

  getters: {
    sortPriceList (state) {
      return state.price.sort((a, b) => a-b)
    }
  },

  mutations: {
    FILL_PRICE_LIST (state, array) {
      state.price = array
    }
  },

  actions: {
    createPriceList ({commit}, response) {
      commit('FILL_PRICE_LIST', response)
    }
  }
}