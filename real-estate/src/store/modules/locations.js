export default {
  state:{
    locations:[]
  },

  mutations: {
    FILL_LOCATIONS (state, array) {
      state.locations = array
    }
  },

  actions: {
    createLocationList ({commit}, response) {
      commit('FILL_LOCATIONS', response)
    }
  }
}