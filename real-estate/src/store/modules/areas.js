export default {
  state: {
    areas:[],
  },

  getters: {
    sortAreaList (state) {
      return state.areas.sort((a, b) => a-b)
    }
  },

  mutations:{
    FILL_AREAS(state, array) {
      state.areas = array
    }
  },

  actions:{
    renderAreaList ({commit}, response) {
      commit('FILL_AREAS', response)
    }
  }
}