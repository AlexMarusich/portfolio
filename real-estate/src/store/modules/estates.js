import axios from "../../axios-requests/axios.js"
import router from '../../router/index.js'

export default {
  state: {
    estates: [],
  },

  mutations:{
    FILL_ESTATES (state, array) {
      state.estates = array
    }
  },

  actions:{
    fillEstateList ({commit}, array) {
      commit('FILL_ESTATES', array)
    },

    renderEstatesList ({dispatch}, filters) {
      let filteredUrl = 'estates?'
      Object.entries(filters).forEach(item => {
        filteredUrl = `${filteredUrl}${item[0]}=${item[1]}&`
      })
      filteredUrl = filteredUrl.substring(0, filteredUrl.length-1)
      axios.getList(filteredUrl)
      .then(response => {
        router.push({name:'Estates', query:filters}).catch(()=>{})
        dispatch('fillEstateList', response.data)
      })
      .catch (error => {
      if (error.status === 404) {
        router.push('/404')
      } else {
        console.log(`Error has been declared ${error}`)
      }
      })
    },
  }
}