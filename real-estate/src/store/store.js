import Vue from 'vue'
import Vuex from 'vuex'
import locations from '@/store/modules/locations.js'
import estates from '@/store/modules/estates.js'
import filters from '@/store/modules/filters.js'
import areas from '@/store/modules/areas.js'
import price from '@/store/modules/price.js'

Vue.use(Vuex)

export default  new Vuex.Store({
  modules:{
    locations,
    estates,
    filters,
    areas,
    price
  }
})