import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/home/Home.vue";
import About from "../views/about/About.vue";
import Contacts from "../views/contacts/Contacts.vue";
import Estates from "../views/estates/Estates.vue";
import EstatePage from "../views/estate-item/EstatePage.vue";
import EstateContainer from "../views/estates/EstateContainer.vue";
import NotFoundPage from "../views/404/NotFoundPage.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/contacts",
    name: "Contacts",
    component: Contacts
  },
  {
    path: "/estates",
    name: "EstateContainer",
    component: EstateContainer,
    children: [
      {
        path: "/",
        name: "Estates",
        component: Estates,
        props: true
      },
      {
        path: "/:id",
        name: "EstatePage",
        component: EstatePage,
        props: true,
      }
    ]
  },
  {
    path: "*",
    name: "notFound",
    component: NotFoundPage,
  },
];

const router = new VueRouter({
  mode: "history",
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve({top: 0})
        }, 200)
      })
    }
  },
  routes,
});

export default router;
