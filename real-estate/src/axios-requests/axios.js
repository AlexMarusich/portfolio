import axios from 'axios'

const apiEstates = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  withCredentials: false,
  headers:{
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {

  getList (path) {
    return apiEstates.get(`/${path}`)
  }
}
