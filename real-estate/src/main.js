import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import '../src/assets/icons/all.css'
import './assets/scss/style.scss';

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
