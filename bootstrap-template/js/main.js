let swiperProduct = new Swiper ('.category-slider', {

    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    slidesPerView: 1,
    breakpoints: {
        500: {
            slidesPerView: 2,
            slidesPerGroup: 2
        },
        768: {
            slidesPerView: 4,
            slidesPerGroup: 4
        },
        992: {
            slidesPerView: 5,
            slidesPerGroup: 5
        },
        1200: {
            slidesPerView: 6,
            slidesPerGroup: 6
        }
    }
});

let swiperReview = new Swiper ('.review-swiper', {

    loop: true,
    spaceBetween: 25,
    pagination: {
        el: '.review-swiper-pagination',
        clickable: true
    },
    slidesPerView: 1,
    breakpoints: {
        768: {
            slidesPerView: 2,
            slidesPerGroup: 2,
        }
    }
});