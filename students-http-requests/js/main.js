// variables of button-tags in "index.html"
let fullListButton = document.getElementById('FullListButton');
let showOneStudentBtn = document.getElementById('showOneStudent');
let showThreeStudentsBtn = document.getElementById('navButton2');
let showStudentToChangeBtn = document.getElementById('showStudentToChange');
let submitChangeOfStudentBtn = document.getElementById('submitChangeOfStudent');
let deleteStudentBtn = document.getElementById('deleteStudent');

// variables of input- and form-tags in "index.html"
let idInputOneStudent = document.getElementById('idInputOneStudent');
let inputStudentToChange = document.getElementById('idStudentToChange');
let inputIdStudentToDelete = document.getElementById('idStudentToDelete');

// variables of div-tags, what student information is going to be created in"
let oneStudentBlock = document.getElementById('oneStudentBlock');
let studentToChangeBlock = document.getElementById('studentToChangeBlock');

let urlString = 'http://localhost:3000/students';
let dataOfCurrentStudent =  null;

let createFullName = function(data, divBlock) {
    let fullNameOfStudent = document.createElement('p');
    fullNameOfStudent.innerHTML = `Full Name: ${data.firstName} ${data.lastName}`;
    divBlock.append(fullNameOfStudent);
}

let createGrade = function(data, divBlock) {
    let gradeOfStudent = document.createElement('span');
    gradeOfStudent.classList.add('d-block');
    gradeOfStudent.classList.add('mb-3');
    gradeOfStudent.innerHTML = ` Grade Point Average: ${data.GPA}`;
    divBlock.append(gradeOfStudent);
}

let createEmail = function(data, divBlock) {
    let emailOfStudent = document.createElement('p');
    emailOfStudent.innerHTML = `Email address: ${data.email}`
    divBlock.append(emailOfStudent);
}

let createId = function(data, divBlock) {
    let idOfStudent = document.createElement('p');
    idOfStudent.innerHTML = `"id" of the student: ${data.id}`;
    divBlock.append(idOfStudent);
}

let createDivBlockForList = function(data, divBlock) {
    let singleStudentBlock = document.createElement('div');
    singleStudentBlock.classList.add('one-student');
    singleStudentBlock.classList.add('mb-3');
    singleStudentBlock.classList.add('mx-auto');
    divBlock.append(singleStudentBlock);

    creatSingleStudentBlock(data, singleStudentBlock);
    createId(data, singleStudentBlock)
}

let creatSingleStudentBlock = function(data, divBlock) {
    createFullName(data, divBlock)
    createGrade(data, divBlock)
    createEmail(data, divBlock)
}

let fillSingleStudentBlock = function(data, divBlock, idInput) {
    divBlock.innerHTML = '';
    idInput.value = '';

    creatSingleStudentBlock(data, divBlock);

    if (divBlock === studentToChangeBlock) {
        dataOfCurrentStudent = data;
    }
}

let checkConditionId = async function(errorBlock, divBlock, idInput, request) {
    let checkListResponse = await fetch(urlString);
    let checkData = await checkListResponse.json();

    if (idInput.value === '') {
        errorBlock.innerHTML = 'Enter the "id" of the student';
    } else {
        if (checkData.find(item => item.id === idInput.value)){
            errorBlock.innerHTML = '';
            let idOfStudent = idInput.value;

            if (request === 'GET') {
                let response = await fetch(urlString + `/${idOfStudent}`);
                let data = await response.json();
    
                fillSingleStudentBlock(data, divBlock, idInput);
    
            } else if (request === 'DELETE') {
                await fetch(urlString + `/${idOfStudent}`, {
                    method:'DELETE'
                });
                idInput.value = '';
            }

        } else if (checkData.find(item => item.id !== idInput.value)) {
            errorBlock.innerHTML = 'There is no student with this "id"';
            idInput.value = '';
        }
    }
}  

let createStudentList = function(data, divBlock) {
    divBlock.innerHTML = '';

    data.forEach(function(item, index) {
        createDivBlockForList(data[index], divBlock);
    })
}

let renderStudentList = async function (count) {
    try {
        let studentListBlock = document.getElementById('studentList');
        let threeStudentsBlock = document.getElementById('threeStudentsBlock');
        let response;
        let data;
        if (count === 'all') {
            response = await fetch(urlString);
            data = await response.json();

            createStudentList(data, studentListBlock)

        } else if (count === 'three') {
            response = await fetch(urlString + `?_page=1&_limit=3`);
            data = await response.json();

            createStudentList(data, threeStudentsBlock);
        }
    } catch (error) {
        console.log(error);
    }
}

let showOneStudent = async function() {
    try {
        let errorMessageSingleStudent = document.querySelector('.error-message');

        checkConditionId(errorMessageSingleStudent, oneStudentBlock, idInputOneStudent, 'GET');
    } catch (error) {
        console.log(error);
    }
}

let showCurrentStudentToChange = async function() {
    try {
        let errorMessageStudentToChange = document.getElementById('errorBlokStudentToChange');
        let errorSubmitChange = document.getElementById('errorSubmitChange');
        errorSubmitChange.innerHTML = '';

        checkConditionId(errorMessageStudentToChange, studentToChangeBlock, inputStudentToChange, 'GET');

    } catch (error) {
        console.log(error);
    }
}

let changeCurrentStudent = async function(event) {
    event.preventDefault();

    let errorSubmitChange = document.getElementById('errorSubmitChange');

    if (!dataOfCurrentStudent) {
        errorSubmitChange.innerHTML = 'Enter the "id" of the student';
    } else {
        let formData = new FormData(changeInfoStudent);
        let values = Object.fromEntries(formData.entries());
        let lengthCounter = 0;
        
         for (let key in values) {
            if (values[key].length) {
                lengthCounter++
                await fetch(urlString + `/${dataOfCurrentStudent.id}`, {
                    method:'PATCH',
                    body: JSON.stringify({
                        [key]: values[key]
                    }),
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8',
                      },
                });
            }
        };

        if (!lengthCounter) {
            errorSubmitChange.innerHTML = 'There is no new data of the student';
        } else {
            errorSubmitChange.innerHTML = '';
            changeInfoStudent.reset();
            let response = await fetch(urlString + `/${dataOfCurrentStudent.id}`);
            let newData = await response.json();
            fillSingleStudentBlock(newData, studentToChangeBlock, inputStudentToChange);
            lengthCounter = 0;
        }
    }
}

let addNewStudent = async function (event) {
    try {
        event.preventDefault();

        let errorMessageNewStudent = document.getElementById('errorBlockNewStudent');
        let response = await fetch(urlString);
        let currentList = await response.json();

        let formData = new FormData(createNewStudent);
        formData.append('id', `${++currentList.length}`)
        let values = Object.fromEntries(formData.entries())

        if (values.firstName ==='') {
            errorMessageNewStudent.innerHTML = 'Enter the First Name of the student';
        } else if (values.lastName ==='') {
            errorMessageNewStudent.innerHTML = 'Enter the Last Name of the student';
        } else if  (values.GPA ==='') {
            errorMessageNewStudent.innerHTML = 'Enter grade point average of the student';
        } else if (values.email ==='') {
            errorMessageNewStudent.innerHTML = 'Enter email of the student';
        } else {
            errorMessageNewStudent.innerHTML = '';
            await fetch(urlString, {
                method:'POST',
                body: JSON.stringify(values),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            });
        }
    } catch (error) {
        console.error(error);
    }
}

let deleteStudent = async function () {
    try {
    let errorMessageDeleteStudent = document.getElementById('errorBlockDeleteStudent');

    checkConditionId(errorMessageDeleteStudent, null, inputIdStudentToDelete, 'DELETE');

    } catch (error) {
        console.error(error);
    }
}

fullListButton.addEventListener('click', function() {renderStudentList('all')});
showOneStudentBtn.addEventListener('click', showOneStudent);
navButton2.addEventListener('click', function() {renderStudentList('three')});
showStudentToChangeBtn.addEventListener('click', showCurrentStudentToChange);
changeInfoStudent.addEventListener('submit', changeCurrentStudent);
createNewStudent.addEventListener('submit', addNewStudent)
deleteStudentBtn.addEventListener('click', deleteStudent);