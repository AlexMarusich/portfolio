let inputOfTask = document.getElementById('taskInput');
let submitTaskBtn = document.getElementById('submitTaskInput');
let toDoWrapp = document.getElementById('toDoWrapp');
let errorMessage = document.getElementById('errorText');

const LOCAL_STORAGE_KEY = 'taskList';
let itemArray = [];

class Task {
    constructor(description) {
        this.description = description;
        this.completed = false;
    }
}

let createHtmlTodoItem = function(index) {
    let todoItemDiv = document.createElement ('div');
    todoItemDiv.classList.add('to-do-item');

    createSpanElement(todoItemDiv, index);
    createCheckBocksElement(todoItemDiv, index);
    createButtonElement(todoItemDiv, index);

    toDoWrapp.append(todoItemDiv);
};

let createSpanElement = function (todoItemDiv, index) {
    let todoItemSpan = document.createElement ('span');
    todoItemSpan.classList.add('text-task');
    todoItemDiv.append(todoItemSpan);
    todoItemSpan.setAttribute('id', `span${index}`);

}

let createCheckBocksElement = function (todoItemDiv, index) {
    let todoItemCheckBocks = document.createElement ('input');
    todoItemCheckBocks.classList.add('check-btn-list');
    todoItemCheckBocks.setAttribute('type', 'checkbox');
    todoItemCheckBocks.setAttribute('onclick', `completeTask(this.id)`);
    todoItemDiv.append(todoItemCheckBocks);
    todoItemCheckBocks.setAttribute('id', `checkBox${index}`);
}

let createButtonElement = function(todoItemDiv, index) {
    let todoItemBtn = document.createElement ('button');
    todoItemBtn.classList.add('delete');
    todoItemBtn.setAttribute('onclick', `deleteTask(this.id)`);
    todoItemDiv.append(todoItemBtn);
    todoItemBtn.setAttribute('id', `button${index}`);

    createIconForButtonElement(todoItemBtn);
}

let createIconForButtonElement = function(todoItemBtn) {
    let todoItemBtnIcon = document.createElement ('i');
    todoItemBtnIcon.classList.add('icon-trash');
    todoItemBtn.append(todoItemBtnIcon);
}

let updateTaskList = function () {
    if (localStorage[LOCAL_STORAGE_KEY]) {
        itemArray = (JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)));
    }
};

let updateLocalStorage = function () {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify (itemArray));
}

let fillToDoWrapp = function () { 
    clearToDoWrapp()

    let initialNumberOfItems = 2;
    
    for (i = 0; i < initialNumberOfItems; i++) {
        createHtmlTodoItem(i);
    };

    itemArray.forEach(function(item, index) {
        if (document.getElementById('span1').innerHTML === '') {
            document.getElementById(`span${index}`).innerHTML = itemArray[index].description;
        } else {
            createHtmlTodoItem(initialNumberOfItems);
            document.getElementById(`span${index}`).innerHTML = itemArray[index].description;
            initialNumberOfItems++
        };
        changeCompleteStyle(index);
    })
}

let clearToDoWrapp = function() {
    toDoWrapp.innerHTML = '';
}

let changeCompleteStyle = function(index) {
    if (itemArray[index].completed) {
        document.getElementById(`span${index}`).classList.add('completed');
        document.getElementById(`checkBox${index}`).setAttribute('checked', '');
    } else {
        document.getElementById(`span${index}`).classList.remove('completed');
        document.getElementById(`checkBox${index}`).removeAttribute('checked', '');
    };
}  

let render = function() {
    updateLocalStorage();
    fillToDoWrapp();
}

let completeTask = function (id) {
    index = id.slice(-1);
    itemArray[index].completed = !itemArray[index].completed;
    render();

}

let deleteTask = function (id) {
    index = id.slice(-1);
    itemArray.splice(index, 1);
    render();
}

submitTaskBtn.addEventListener('click', function () {
    if (inputOfTask.value === '') {
        errorMessage.innerHTML = 'Input is empty';
        return
    } else if (inputOfTask.value.length < 12) {
        errorMessage.innerHTML = 'Input task is less than 12 litters';
        return
    } else {
        let task = new Task(inputOfTask.value);
        itemArray.push(task);
        inputOfTask.value = '';
        render();
    };
})

inputOfTask.addEventListener('keydown', function () {
        errorMessage.innerHTML = '';
})

updateTaskList();
render();
