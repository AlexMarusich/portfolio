let swiperReview = new Swiper ('.team-slider', {

    loop: true,
    pagination: {
        el: '.team-swiper-pagination',
        clickable: true
    },
    slidesPerView: 1,
});

let navMenuButton = document.getElementById('navMenuButton');
let morePublications = document.getElementById('morePublications');
let moveToTeam = document.getElementById('moveToTeam');

let changeNavItem = function (currentContentItem) {
    let navItemList = document.querySelectorAll('.nav-link' );
    navItemList.forEach(function(item, index) {
        if (navItemList[index].classList.contains('nav-active')) {
            navItemList[index].classList.remove('nav-active')
        }
    })
    navItemList[currentContentItem].classList.add('nav-active')
};

let changeContentItem = function(currentContentItem) {
    let contentItemList = document.querySelectorAll('.tab-content-item');

    if (!contentItemList[currentContentItem].classList.contains('content-show')) {
        contentItemList.forEach(function(item, index) {
            if (contentItemList[index].classList.contains('content-show')) {
                contentItemList[index].classList.remove('content-show');
                contentItemList[index].classList.add('content-hidden');
            }
            setTimeout(function() {
            contentItemList[index].classList.remove('content-hidden');
                }, 200);
        })
        setTimeout(function() {
            contentItemList[currentContentItem].classList.add('content-show');
        }, 200);
    }
}

let changeTab = function(id) {
    let currentContentItem = id.slice(-1);

    changeNavItem(currentContentItem);

    changeContentItem(currentContentItem);
}

let changeNavMenu = function() {
    let navBar = document.getElementById('navBarMenu');

    if (navBar.classList.contains('navbar-hidden')) {
        navBar.classList.remove('navbar-hidden');
        navBar.classList.add('navbar-show');

        for (i = 1; i <= 3; i++ ) {
            let minus = document.getElementById(`minus${i}`);
            minus.classList.add(`clicked-minus-${i}`);
        }
    } else {
        navBar.classList.remove('navbar-show');
        navBar.classList.add('navbar-hidden');

        for (i = 1; i <= 3; i++ ) {
            let minus = document.getElementById(`minus${i}`);
            minus.classList.remove(`clicked-minus-${i}`);
        }
    }
}

let changeNewsSection = function() {    
    let news = document.getElementById('news');
    news.classList.toggle('news-wrapper-open');

    let newsShadow = document.querySelector('.shadow');
    newsShadow.classList.toggle('shadow-hidden');

    morePublications.classList.toggle('more-publications-relative');

    if (morePublications.innerHTML !== 'Приховати') {
        morePublications.innerHTML = 'Приховати';
    } else {
        morePublications.innerHTML = 'Ще публікації';
    };
}

let scrollToTeamSection = function() {
    let teamSection = document.getElementById('teamSection');
    teamSection.scrollIntoView({block:"start", behavior:"smooth"});
}

navMenuButton.addEventListener('click', changeNavMenu);

morePublications.addEventListener('click', changeNewsSection);

moveToTeam.addEventListener('click', scrollToTeamSection);