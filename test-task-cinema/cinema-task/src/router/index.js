import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/home/Home.vue";
import BookingScene from "../views/bookingScene/BookingScene.vue";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "movies",
    component: Home,
    props: true
  },
  {
    path:"/:movieSlug/:id",
    name: "BookingScene",
    movieName: 'name',
    props: true,
    component: BookingScene
  }
];

const router = new VueRouter({
  routes,
});

export default router;
