export default {
  movies: [
    {
      name: 'AVATAR 2: The Way of Water',
      slug: 'avatar',
      id: '1',
      image: 'avatar.jpg',
      sessions: [
        {
          time: '13:10',
          price: 100,
          id:1.1,
        },
        {
          time: '17:50',
          price: 100,
          id:1.2,
        },
        {
          time: '21:20',
          price: 80,
          id:1.3,
        }
      ]
    },
    {
      name: 'Batman',
      slug: 'batman',
      id: '2',
      image: 'batman.jpg',
      sessions: [
        {
          time: '17:50',
          price: 100,
          id:2.1,
        },
        {
          time: '21:20',
          price: 80,
          id:2.2,
        },
      ]
    },
    {
      name: 'Black Panther: Wakanda Forever',
      slug: 'black-panther',
      id: '3',
      image: 'black-panther.jpg',
      sessions: [
        {
          time: '11:30',
          price: 100,
          id:3.1,
        },
        {
          time: '14:20',
          price: 100,
          id:3.2,
        },
        {
          time: '17:50',
          price: 100,
          id:3.3,
        },
        {
          time: '21:20',
          price: 80,
          id:3.4,
        },
      ]
    },
    {
      name: 'Spider-Man: Across the Spider-Verse',
      slug: 'spider-man',
      id: '4',
      image: 'spider-man.jpg',
      sessions: [
        {
          time: '11:30',
          price: 100,
          id:4.1,
        },
        {
          time: '14:20',
          price: 100,
          id:4.2,
        }
      ]
    },
    {
      name: 'Uncharted',
      slug: 'uncharted',
      id: '5',
      image: 'uncharted.jpg',
      sessions: [
        {
          time: '13:10',
          price: 100,
          id:5.1,
        },
        {
          time: '17:50',
          price: 100,
          id:5.2,
        },
        {
          time: '21:20',
          price: 80,
          id:5.3,
        }
      ]
    }
  ]
}